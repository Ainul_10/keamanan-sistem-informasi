from django.shortcuts import render
from .models import User

# Create your views here.
def index(request):
    return render(request, 'ujian_aplikasi_1174073/index_1174073.html')


def userViews(request):
    pengguna = User.objects.all()
    return render(request, 'ujian_aplikasi_1174073/users_1174073.html', {'pengguna' : pengguna})

